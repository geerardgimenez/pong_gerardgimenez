class Menu extends Phaser.Scene {
    constructor() {
        super({ key: 'Menu', active: true });
        self.partida;
        self.configuracio;
        self.credits;
    }

    preload() {

    }

    create() {
        self.partida = this.add.text(275, 200, 'Començar partida', { font: '20px Courier', fill: '#00ff00' });
        partida.setInteractive();

        self.configuracio = this.add.text(275, 250, 'Configuracio', { font: '20px Courier', fill: '#00ff00' });
        configuracio.setInteractive();

        self.credits = this.add.text(275, 300, 'Credits', { font: '20px Courier', fill: '#00ff00' });
        credits.setInteractive();

        partida.on('pointerdown', function (event) {
            if (this.scene.get('Joc')) {
                game.scene.start('Joc');
                game.scene.wake('Botons');
                game.scene.sleep('Menu');
            } else {
                game.scene.add('Joc', Joc);
                game.scene.add('Botons', Botons);
                game.scene.start('Joc');
                game.scene.start('Botons');
                game.scene.sleep('Menu');
            }
        }, this);

        configuracio.on('pointerdown', function (event) {
            if (this.scene.get('Configuracio')) {
                game.scene.wake('Configuracio');
                game.scene.sleep('Menu');
            } else {
                game.scene.add('Configuracio', Configuracio);
                game.scene.start('Configuracio');
                game.scene.sleep('Menu');
            }
        }, this);
        credits.on('pointerdown', function (event) {
            if (this.scene.get('Credits')) {
                game.scene.wake('Credits');
                game.scene.sleep('Menu');
            } else {
                game.scene.add('Credits', Credits);
                game.scene.start('Credits');
                game.scene.sleep('Menu');
            }
        }, this);


    }

    update(time, delta) {

    }
}

class Configuracio extends Phaser.Scene {
    constructor() {
        super({ key: 'Configuracio', active: true });
        self.botoTornar;
        self.textpuntuacio;
        self.puntuacio3;
        self.puntuacio5;
        self.puntuacio10;
        self.puntuaciototal;

        self.quadrat;
        self.quadrat2;
        self.quadrat3;
        self.quadrat4;

        self.textforma1;
        self.migcercle;
        self.concau;
        self.rectangle;
        self.forma1

        self.textforma2;
        self.migcercle2;
        self.concau2;
        self.rectangle2;
        self.forma2

        self.obstacles;
        self.obssi;
        self.obsno;
        self.obstaclesfinal;

    }

    preload() {
        this.load.image('tornar', 'assets/retroceder.png');
        this.load.image('quadrat', 'assets/quadrat.png');
        this.load.image('jugador1', 'assets/rectangle.png');
        this.load.image('jugador2', 'assets/rectangle.png');
        this.load.image('migcercle', 'assets/migcircle.png');
        this.load.image('migcercle2', 'assets/migcircle2.png');
        this.load.image('concau', 'assets/concau.png');
        this.load.image('concau2', 'assets/concau2.png');
        this.load.image('rectangle', 'assets/rectangle.png');
        this.load.image('rectangle2', 'assets/rectangle2.png');

    }

    create() {
        self.botoTornar = this.matter.add.image(80, 80, 'tornar');
        botoTornar.setInteractive();
        botoTornar.setScale(.2);

        botoTornar.on('pointerdown', function (event) {
            game.scene.wake('Menu');
            game.scene.sleep('Configuracio');
        }, this)

        self.textpuntuacio = this.add.text(50, 150, 'Puntuació: ', { font: '20px Courier', fill: '#00ff00' });
        self.puntuacio3 = this.add.text(225, 150, '3', { font: '20px Courier', fill: '#00ff00' });
        puntuacio3.setInteractive();

        puntuacio3.on('pointerdown', function (event) {
            self.puntuaciototal = "3";
            if (self.quadrat) {
                quadrat.x = 232;
                quadrat.y = 160;
            } else {
                self.quadrat = this.add.image(232, 160, 'quadrat');
                quadrat.setScale(.06);
            }
        }, this)

        self.puntuacio5 = this.add.text(400, 150, '5', { font: '20px Courier', fill: '#00ff00' });
        puntuacio5.setInteractive();
        puntuacio5.on('pointerdown', function (event) {
            self.puntuaciototal = "5";
            if (self.quadrat) {
                quadrat.x = 408;
                quadrat.y = 160;
            } else {
                self.puntuaciototal = puntuacio5;
                self.quadrat = this.add.image(408, 160, 'quadrat');
                quadrat.setScale(.06);
            }
        }, this)
        self.puntuacio10 = this.add.text(575, 150, '10', { font: '20px Courier', fill: '#00ff00' });
        puntuacio10.setInteractive();
        puntuacio10.on('pointerdown', function (event) {
            self.puntuaciototal = "10";
            if (self.quadrat) {
                quadrat.x = 588;
                quadrat.y = 160;
            } else {
                self.puntuaciototal = puntuacio10;
                self.quadrat = this.add.image(588, 160, 'quadrat');
                quadrat.setScale(.06);
            }
        }, this)

        self.textforma1 = this.add.text(50, 250, 'Forma Jugador 1: ', { font: '20px Courier', fill: '#00ff00' });
        self.migcercle = this.add.image(350, 250, 'migcercle');
        migcercle.setScale(.11);
        migcercle.setInteractive();

        migcercle.on('pointerdown', function (event) {
            self.forma1 = "0";
            if (self.quadrat2) {
                quadrat2.x = 346;
                quadrat2.y = 250;
            } else {
                self.quadrat2 = this.add.image(346, 250, 'quadrat');
                quadrat2.setScale(.1);
            }
        }, this)

        self.concau = this.matter.add.image(500, 250, 'concau');
        concau.setScale(.06);
        concau.setInteractive();

        concau.on('pointerdown', function (event) {
            self.forma1 = "1";
            if (self.quadrat2) {
                quadrat2.x = 500;
                quadrat2.y = 250;
            } else {
                self.quadrat2 = this.add.image(500, 250, 'quadrat');
                quadrat2.setScale(.1);
            }
        }, this)

        self.rectangle = this.matter.add.image(650, 250, 'rectangle');
        rectangle.setScale(.32, .13);
        rectangle.rotation = 90.0 * (3.1459 / 180);
        rectangle.setInteractive();

        rectangle.on('pointerdown', function (event) {
            self.forma1 = "2";
            if (self.quadrat2) {
                quadrat2.x = 650;
                quadrat2.y = 250;
            } else {
                self.quadrat2 = this.add.image(650, 250, 'quadrat');
                quadrat2.setScale(.1);
            }
        }, this)

        self.textforma2 = this.add.text(50, 350, 'Forma Jugador 2: ', { font: '20px Courier', fill: '#00ff00' });
        self.migcercle2 = this.matter.add.image(350, 350, 'migcercle2');
        migcercle2.setScale(.11);
        migcercle2.setInteractive();
        migcercle2.on('pointerdown', function (event) {
            self.forma2 = "0";
            if (self.quadrat3) {
                quadrat3.x = 346;
                quadrat3.y = 350;
            } else {
                self.quadrat3 = this.add.image(346, 350, 'quadrat');
                quadrat3.setScale(.1);
            }
        }, this)

        self.concau2 = this.matter.add.image(500, 350, 'concau2');
        concau2.setScale(.06);
        concau2.setInteractive();
        concau2.on('pointerdown', function (event) {
            self.forma2 = "1";
            if (self.quadrat3) {
                quadrat3.x = 500;
                quadrat3.y = 350;
            } else {
                self.quadrat3 = this.add.image(500, 350, 'quadrat');
                quadrat3.setScale(.1);
            }
        }, this)
        self.rectangle2 = this.matter.add.image(650, 350, 'rectangle2');
        rectangle2.setScale(.32, .13);
        rectangle2.rotation = 90.0 * (3.1459 / 180);
        rectangle2.setInteractive();
        rectangle2.on('pointerdown', function (event) {
            self.forma2 = "2";
            if (self.quadrat3) {
                quadrat3.x = 650;
                quadrat3.y = 350;
            } else {
                self.quadrat3 = this.add.image(650, 350, 'quadrat');
                quadrat3.setScale(.1);
            }
        }, this)

        self.obstacles = this.add.text(50, 450, 'Obstacles: ', { font: '20px Courier', fill: '#00ff00' });
        self.obssi = this.add.text(300, 450, 'Si ', { font: '20px Courier', fill: '#00ff00' });
        obssi.setInteractive();
        obssi.on('pointerdown', function (event) {
            self.obstaclesfinal = "1";
            if (self.quadrat4) {
                quadrat4.x = 310;
                quadrat4.y = 460;
            } else {
                self.quadrat4 = this.add.image(310, 460, 'quadrat');
                quadrat4.setScale(.1);
            }
        }, this)
        self.obsno = this.add.text(500, 450, 'No ', { font: '20px Courier', fill: '#00ff00' });
        obsno.setInteractive();
        obsno.on('pointerdown', function (event) {
            self.obstaclesfinal = "0";
            if (self.quadrat4) {
                quadrat4.x = 510;
                quadrat4.y = 460;
            } else {
                self.quadrat4 = this.add.image(510, 460, 'quadrat');
                quadrat4.setScale(.1);
            }
        }, this)
    }

    update(time, delta) {

    }
}

class Credits extends Phaser.Scene {
    constructor() {
        super({ key: 'Credits', active: true });
        self.botoTornar;
        self.autor;
        self.explicacio;
        self.explicacio2;
        self.explicacio3;
    }

    preload() {
        this.load.image('tornar', 'assets/retroceder.png');

    }

    create() {
        self.botoTornar = this.matter.add.image(100, 100, 'tornar');
        botoTornar.setInteractive();
        botoTornar.setScale(.2);

        botoTornar.on('pointerdown', function (event) {
            game.scene.wake('Menu');
            game.scene.sleep('Credits');
        }, this)

        self.tornar = this.add.text(200, 200, 'Autor: Gerard Gímenez.', { font: '20px Courier', fill: '#00ff00' });
        self.explicacio = this.add.text(50, 250, 'El joc consisteix en el classic joc de ping pong,', { font: '20px Courier', fill: '#00ff00' });
        self.explicacio2 = this.add.text(50, 275, 'amb algunes variants com la inserció', { font: '20px Courier', fill: '#00ff00' });
        self.explicacio3 = this.add.text(50, 300, 'de obstacles i/o bonificacions a certs nivells', { font: '20px Courier', fill: '#00ff00' });
    }

    update(time, delta) {

    }
}

class Joc extends Phaser.Scene {
    constructor() {
        super({ key: 'Joc', active: true });
        var cursor;
        self.pilota;
        self.pala;
        self.velocitat = 1000;
        self.lletraW;
        self.lletraS;
        self.v;
        self.puntuacio1;
        self.puntuacio2;
        self.contador1 = 0;
        self.contador2 = 0;
        self.configuracio;
        self.forma1;
        self.forma2;
        self.obstaclesfinal;
        self.puntuaciototal;
    }

    preload() {
        this.load.image('pilota', 'assets/ball.png');
        this.load.image('migcercle', 'assets/migcircle.png');
        this.load.image('migcercle2', 'assets/migcircle2.png');
        this.load.image('concau', 'assets/concau.png');
        this.load.image('concau2', 'assets/concau2.png');
        this.load.image('rectangle', 'assets/rectangle.png');
        this.load.image('rectangle2', 'assets/rectangle2.png');
        this.load.image('obstacle', 'assets/obstacle.png');
    }

    create() {
        //Posar limits al joc, pots posarho dins el parentesis
        this.matter.world.setBounds();

        self.configuracio = this.scene.get('Configuracio')
        self.pilota = this.matter.add.image(400, 300, 'pilota');
        if (!self.forma1) {
            self.forma1 = 2;
        } if (!self.forma2) {
            self.forma2 = 2;
        } if (!self.obstaclesfinal) {
            self.obstaclesfinal = 0;
        } if (!self.puntuaciototal) {
            self.puntuaciototal = 3;
        }

        if (forma1 == "0") {
            self.pala = this.matter.add.image(25, 300, 'migcercle');
            pala.setScale(.18);
        } else if (forma1 == '1') {
            self.pala = this.matter.add.image(25, 300, 'concau');
            pala.setScale(.1);
        } else if (forma1 == '2') {
            self.pala = this.matter.add.image(25, 300, 'rectangle');
            pala.setScale(.5, .2);
            pala.rotation = 90.0 * (3.1459 / 180);
        }

        if (forma2 == "0") {
            self.pala2 = this.matter.add.image(775, 300, 'migcercle2');
            pala2.setScale(.18);
            pala2.rotation = 180 * (3.1459 / 180);
        } else if (forma2 == '1') {
            self.pala2 = this.matter.add.image(775, 300, 'concau2');
            pala2.setScale(.1);
            pala2.rotation = 180 * (3.1459 / 180);
        } else if (forma2 == '2') {
            self.pala2 = this.matter.add.image(775, 300, 'rectangle2');
            pala2.setScale(.5, .2);
            pala2.rotation = 90.0 * (3.1459 / 180);
        }


        pilota.setCircle();
        pilota.setScale(.2);
        pilota.setVelocity(10);
        pilota.setBounce(1);


        pala.body.isStatic = true;
        pala2.body.isStatic = true;


        self.cursor = this.input.keyboard.createCursorKeys();
        self.lletraW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
        self.lletraS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);

        self.puntuacio1 = this.add.text(350, 550, '0', { font: '42px Courier', fill: 'blue' });
        self.puntuacio2 = this.add.text(400, 550, '0', { font: '42px Courier', fill: 'red' });


        if (obstaclesfinal == '1') {
            for (var index = 0; index < 6; index++) {
                var x = Phaser.Math.Between(125, 575)
                var y = Phaser.Math.Between(50, 500)

                var obstacle = this.matter.add.image(x, y, 'obstacle');
                var c = Phaser.Math.FloatBetween(0, Math.PI * 2);

                obstacle.setScale(.06);
                obstacle.rotation = c;
                obstacle.setInteractive();
                obstacle.body.isStatic = true;

            }
        }

}

update(time, delta) {
    self.configuracio = this.scene.get('Configuracio')
    var velocitatPilota = 15;
    if (lletraW.isDown && pala.y > 55) {
        pala.y = pala.y - velocitat * delta / 1000;
    } else if (lletraS.isDown && pala.y < 545) {
        pala.y = pala.y + velocitat * delta / 1000;
    }




    if (cursor.up.isDown && pala2.y > 55) {
        pala2.y = pala2.y - velocitat * delta / 1000;
    } else if (cursor.down.isDown && pala2.y < 545) {
        pala2.y = pala2.y + velocitat * delta / 1000;
    }

    if (pilota.x <= 25.6) {
        contador2++;
        puntuacio2.setText(contador2);
    } else if (pilota.x > 774.4) {
        contador1++;
        puntuacio1.setText(contador1);
    }

    if (contador1 >= puntuaciototal || contador2 >= puntuaciototal) {
        if (this.scene.get('Vencedor')) {
            game.scene.wake('Vencedor');
            game.scene.start('Vencedor');
            game.scene.sleep('Joc');
            game.scene.sleep('Botons');
            puntuacio1.setText('0');
            puntuacio2.setText('0');

        } else {
            game.scene.add('Vencedor', Vencedor);
            game.scene.start('Vencedor');
            game.scene.sleep('Joc');
            game.scene.sleep('Botons');
            puntuacio1.setText('0');
            puntuacio2.setText('0');
        }

    }
    self.v = new Phaser.Math.Vector2(pilota.body.velocity);

    if (v.x < 1.5 && v.x > 0) {
        v.x = 5;
    } else if (v.x < 0 && v.x > -1.5) {
        v.x = -5;
    }
    if (v.x == 0 && v.y == 0) {
        pilota.setVelocity(10);
    } else {
        var velNova = v.normalize();
        velNova.scale(velocitatPilota);
    }
    pilota.setVelocity(velNova.x, velNova.y);
}
}

class Vencedor extends Phaser.Scene {
    constructor() {
        super({ key: 'Vencedor', active: true });
        self.botoTornar;
        self.botoComençar;
        self.joc;
        self.vencedor;
    }

    preload() {
        this.load.image('tornar', 'assets/retroceder.png');
        this.load.image('play', 'assets/play.png');
    }

    create() {
        self.joc = this.scene.get('Joc')
        if (contador1 > contador2) {
            self.vencedor = this.add.text(200, 275, 'El jugador blau ha guanyat!', { font: '25px Courier', fill: 'blue' });
        } else if (contador2 > contador1) {
            self.vencedor = this.add.text(200, 275, 'El jugador vermell ha guanyat!', { font: '25px Courier', fill: 'red' });
        }
        self.botoTornar = this.matter.add.image(400, 150, 'tornar');
        botoTornar.setInteractive();
        botoTornar.setScale(.2);

        self.botoComençar = this.matter.add.image(400, 400, 'play');
        botoComençar.setInteractive();
        botoComençar.setScale(.2);

        botoTornar.on('pointerdown', function (event) {
            game.scene.start('Menu');
            //this.scene.setVisible(false, 'Joc');
            game.scene.stop('Joc');
            this.scene.setVisible(false, 'Botons');
            this.scene.setVisible(false, 'Vencedor');
            this.scene.sleep('Vencedor');
            contador1 = 0;
            contador2 = 0;
        }, this)

        botoComençar.on('pointerdown', function (event) {
            this.scene.stop('Joc');
            this.scene.start('Joc');
            this.scene.sleep('Vencedor');
            this.scene.setVisible(false, 'Menu');
            this.scene.setVisible(false, 'Botons');
            contador1 = 0;
            contador2 = 0;
            pilota.x = 400;
            pilota.y = 300;
        }, this)

    }

    update(time, delta) {

    }
}
class Botons extends Phaser.Scene {
    constructor() {
        super({ key: 'Botons', active: true });
        self.botoPausa;
        self.botoStart;
    }

    preload() {

    }

    create() {
        self.botoPausa = this.add.text(350, 20, 'Pause');
        botoPausa.setInteractive();

        botoPausa.on('pointerdown', function (event) {
            game.scene.pause("Joc")
            self.botoStart = this.add.text(420, 20, 'Start');
            botoStart.setInteractive();
            botoStart.on('pointerdown', function (event) {
                game.scene.resume("Joc");
            }, this)


        }, this)
    }

    update(time, delta) {

    }
}
var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#000000',
    scene: [Menu],
    physics: {
        default: 'matter',
        matter: {
            debug: true,
            gravity: { x: 0, y: 0 }
        }
    }
};

var game = new Phaser.Game(config);