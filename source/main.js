
var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#000000',
    scene: [Menu],
    physics: {
        default: 'matter',
        matter: {
            debug: false,
            gravity: { x: 0, y: 0 }
        }
    }
};

var game = new Phaser.Game(config);
