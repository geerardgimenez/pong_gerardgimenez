class Credits extends Phaser.Scene {
    constructor() {
        super({ key: 'Credits', active: true });
        self.botoTornar;
        self.autor;
        self.explicacio;
        self.explicacio2;
        self.explicacio3;
    }

    preload() {
        this.load.image('tornar', 'assets/retroceder.png');

    }

    create() {
        self.botoTornar = this.matter.add.image(100, 100, 'tornar');
        botoTornar.setInteractive();
        botoTornar.setScale(.2);

        botoTornar.on('pointerdown', function (event) {
            game.scene.wake('Menu');
            game.scene.sleep('Credits');
        }, this)

        self.tornar = this.add.text(200, 200, 'Autor: Gerard Gímenez.', { font: '20px Courier', fill: '#00ff00' });
        self.explicacio = this.add.text(50, 250, 'El joc consisteix en el classic joc de ping pong,', { font: '20px Courier', fill: '#00ff00' });
        self.explicacio2 = this.add.text(50, 275, 'amb algunes variants com la inserció', { font: '20px Courier', fill: '#00ff00' });
        self.explicacio3 = this.add.text(50, 300, 'de obstacles i/o bonificacions a certs nivells', { font: '20px Courier', fill: '#00ff00' });
    }

    update(time, delta) {

    }
}