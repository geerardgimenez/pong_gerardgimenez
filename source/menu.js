class Menu extends Phaser.Scene {
    constructor() {
        super({ key: 'Menu', active: true });
        self.partida;
        self.configuracio;
        self.credits;
        self.nom
    }

    preload() {

    }

    create() {
        self.nom = this.add.text(250, 150, 'PONG GAME', { font: '45px Courier', fill: ' #ffffff'});

        self.partida = this.add.text(275, 225, 'Començar partida', { font: '20px Courier', fill: '#00ff00' });
        partida.setInteractive();

        self.configuracio = this.add.text(290, 275, 'Configuracio', { font: '20px Courier', fill: '#00ff00' });
        configuracio.setInteractive();

        self.credits = this.add.text(312, 325, 'Credits', { font: '20px Courier', fill: '#00ff00' });
        credits.setInteractive();

        partida.on('pointerdown', function (event) {
            if (this.scene.get('Joc')) {
                game.scene.stop('Joc');
                game.scene.start('Joc');
                game.scene.start('Botons');
                game.scene.sleep('Menu');
            } else {
                game.scene.add('Joc', Joc);
                game.scene.add('Botons', Botons);
                game.scene.start('Joc');
                game.scene.start('Botons');
                game.scene.sleep('Menu');
            }
        }, this);

        configuracio.on('pointerdown', function (event) {
            if (this.scene.get('Configuracio')) {
                game.scene.wake('Configuracio');
                game.scene.sleep('Menu');
            } else {
                game.scene.add('Configuracio', Configuracio);
                game.scene.start('Configuracio');
                game.scene.sleep('Menu');
            }
        }, this);
        credits.on('pointerdown', function (event) {
            if (this.scene.get('Credits')) {
                game.scene.wake('Credits');
                game.scene.sleep('Menu');
            } else {
                game.scene.add('Credits', Credits);
                game.scene.start('Credits');
                game.scene.sleep('Menu');
            }
        }, this);


    }

    update(time, delta) {

    }
}