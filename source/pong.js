var config = {
    type: Phaser.AUTO, //seleccionar canvas, ell decidirà si utilitzar canvas o webgl
    width: 800, //Amplada
    height: 600, //Altura
    scene: {
        preload: preload, //Funcio que carregarà les fotos o objectes abans de iniciar el joc
        create: create, //Inicia el joc
        update: update
    },
    physics: {
        default: 'matter',
        matter: {
            debug: false,
            gravity: { x: 0, y: 0 }
        }
    }
};
var game = new Phaser.Game(config);


function preload() {
    this.load.image('pilota', 'assets/ball.png');
    this.load.image('pala', 'assets/rectangle.png');
    this.load.image('pala2', 'assets/rectangle.png');
}

var cursor;
var pilota;
var pala;
var velocitat = 1000;
var lletraW;
var lletraS;
var v;
var botoPausa;
var botoStart;
var puntuacio1;
var puntuacio2;
var contador1 = 0;
var contador2 = 0;

function create() {
    //Posar limits al joc, pots posarho dins el parentesis
    this.matter.world.setBounds();

    pilota = this.matter.add.image(400, 300, 'pilota');
    pala = this.matter.add.image(25, 300, 'pala');
    pala2 = this.matter.add.image(775, 300, 'pala2');

    pilota.setCircle();
    pilota.setScale(.2);
    pilota.setVelocity(10);
    pilota.setBounce(1);

    pala.setScale(.5, .2);
    pala.rotation = 90.0 * (3.1459 / 180);
    pala.body.isStatic = true;

    pala2.setScale(.5, .2);
    pala2.rotation = 90.0 * (3.1459 / 180);
    pala2.body.isStatic = true;
    var color = Phaser.Display.Color.GetColor(255,0,0);
    pala2.setTint(color);


    cursor = this.input.keyboard.createCursorKeys();
    lletraW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    lletraS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);

    botoPausa = this.add.text(350, 20, 'Pausar');
    botoPausa.setInteractive();

    botoPausa.on('pointerdown', function(event){
        game.scene.pause("default")
        botoStart = this.add.text(420, 20, 'Start');
        botoStart.setInteractive();
        console.log("prova2")
        botoStart.on('pointerdown', function(event){
               console.log("prova")
        }, this)
        

    }, this)

    puntuacio1 = this.add.text(350, 550, '0', {font: '42px Courier', fill: 'blue'});
    puntuacio2 = this.add.text(400, 550, '0', {font: '42px Courier', fill: 'red'});

  


}

function update(time, delta) {
    
    var velocitatPilota = 15;
        if (lletraW.isDown && pala.y > 55) {
            pala.y = pala.y - velocitat * delta / 1000;
        } else if (lletraS.isDown && pala.y < 545) {
            pala.y = pala.y + velocitat * delta / 1000;
        }

   


    if (cursor.up.isDown && pala2.y > 55) {
        pala2.y = pala2.y - velocitat * delta / 1000;
    } else if (cursor.down.isDown && pala2.y < 545) {
        pala2.y = pala2.y + velocitat * delta / 1000;
    }

    if(pilota.x <= 25.6){
        contador2++;
        puntuacio2.setText(contador2);
    }else if(pilota.x > 774.4){
        contador1++;
        puntuacio1.setText(contador1);
    }

    if (contador1 >= 21){
        var guanyador1 = this.add.text(200, 300, 'El guanyador és el jugador blau !', {font: '21px Courier', fill: '#00ff00'});
        game.scene.pause("default");
    }else if(contador2 >= 21){
        var guanyador2 = this.add.text(200, 300, 'El guanyador és el jugador vermell !', {font: '21px Courier', fill: '#00ff00'});
        game.scene.pause("default");
    }
    v = new Phaser.Math.Vector2(pilota.body.velocity);

    if (v.x < 1.5 && v.x > 0){
       v.x = 5;
    }else if(v.x < 0 && v.x > -1.5){
        v.x = -5;
    }
    if (v.x == 0 && v.y == 0) {
        pilota.setVelocity(10);
    }else{
        var velNova = v.normalize();
        velNova.scale(velocitatPilota);
    }
    pilota.setVelocity(velNova.x, velNova.y);
    

    

}

