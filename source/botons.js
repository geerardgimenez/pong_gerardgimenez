class Botons extends Phaser.Scene {
    constructor() {
        super({ key: 'Botons', active: true });
        self.botoPausa;
        self.botoStart;
        self.botoMenu;
    }

    preload() {

    }

    create() {
        self.botoPausa = this.add.text(360, 20, 'Pausar', { font: '20px Courier', fill: '#00ff00' } );
        botoPausa.setInteractive();

        botoPausa.on('pointerdown', function (event) {
            game.scene.pause("Joc")
            self.botoStart = this.add.text(360, 200, 'Continuar', { font: '25px Courier', fill: '#00ff00' } );
            botoStart.setInteractive();
            botoStart.on('pointerdown', function (event) {
                game.scene.resume("Joc");
                game.scene.stop("Botons");
                game.scene.start("Botons");
            }, this)
            
        
            self.botoMenu = this.add.text(360, 250, 'Menu', { font: '25px Courier', fill: '#00ff00' });
            botoMenu.setInteractive();
            botoMenu.on('pointerdown', function (event){
                game.scene.wake("Menu");
                game.scene.stop("Joc");
                game.scene.stop("Botons");
                contador1 = 0;
                contador2 = 0;
                puntuaciototal = 0;
            }, this)

        }, this)
    }

    update(time, delta) {

    }
}