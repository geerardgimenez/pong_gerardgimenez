class Joc extends Phaser.Scene {
    constructor() {
        super({ key: 'Joc', active: true });
        var cursor;
        self.pilota;
        self.pala;
        self.velocitat = 1000;
        self.lletraW;
        self.lletraS;
        self.v;
        self.puntuacio1;
        self.puntuacio2;
        self.contador1 = 0;
        self.contador2 = 0;
        self.configuracio;
        self.forma1;
        self.forma2;
        self.obstaclesfinal;
        self.puntuaciototal;
    }

    preload() {
        this.load.image('pilota', 'assets/ball.png');
        this.load.image('migcercle', 'assets/migcircle.png');
        this.load.image('migcercle2', 'assets/migcircle2.png');
        this.load.image('concau', 'assets/concau.png');
        this.load.image('concau2', 'assets/concau2.png');
        this.load.image('rectangle', 'assets/rectangle.png');
        this.load.image('rectangle2', 'assets/rectangle2.png');
        this.load.image('obstacle', 'assets/obstacle.png');
    }

    create() {
        //Posar limits al joc, pots posarho dins el parentesis
        this.matter.world.setBounds();
        self.configuracio = this.scene.get('Configuracio')
        self.pilota = this.matter.add.image(400, 300, 'pilota');
        if (!self.forma1) {
            self.forma1 = 2;
        } if (!self.forma2) {
            self.forma2 = 2;
        } if (!self.obstaclesfinal) {
            self.obstaclesfinal = 0;
        } if (!self.puntuaciototal) {
            self.puntuaciototal = 3;
        }

        if (forma1 == "0") {
            self.pala = this.matter.add.image(25, 300, 'migcercle');
            pala.setScale(.18);
        } else if (forma1 == '1') {
            self.pala = this.matter.add.image(25, 300, 'concau');
            pala.setScale(.1);
        } else if (forma1 == '2') {
            self.pala = this.matter.add.image(25, 300, 'rectangle');
            pala.setScale(.5, .2);
            pala.rotation = 90.0 * (3.1459 / 180);
        }

        if (forma2 == "0") {
            self.pala2 = this.matter.add.image(775, 300, 'migcercle2');
            pala2.setScale(.18);
            pala2.rotation = 180 * (3.1459 / 180);
        } else if (forma2 == '1') {
            self.pala2 = this.matter.add.image(775, 300, 'concau2');
            pala2.setScale(.1);
            pala2.rotation = 180 * (3.1459 / 180);
        } else if (forma2 == '2') {
            self.pala2 = this.matter.add.image(775, 300, 'rectangle2');
            pala2.setScale(.5, .2);
            pala2.rotation = 90.0 * (3.1459 / 180);
        }


        pilota.setCircle();
        pilota.setScale(.2);
        pilota.setVelocity(10);
        pilota.setBounce(1);


        pala.body.isStatic = true;
        pala2.body.isStatic = true;


        self.cursor = this.input.keyboard.createCursorKeys();
        self.lletraW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
        self.lletraS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);

        self.puntuacio1 = this.add.text(350, 550, '0', { font: '42px Courier', fill: 'blue' });
        self.puntuacio2 = this.add.text(400, 550, '0', { font: '42px Courier', fill: 'red' });


        if (obstaclesfinal == '1') {
            for (var index = 0; index < 6; index++) {
                var x = Phaser.Math.Between(125, 575)
                var y = Phaser.Math.Between(50, 500)

                var obstacle = this.matter.add.image(x, y, 'obstacle');
                var c = Phaser.Math.FloatBetween(0, Math.PI * 2);

                obstacle.setScale(.06);
                obstacle.rotation = c;
                obstacle.setInteractive();
                obstacle.body.isStatic = true;

            }
        }

}

update(time, delta) {
    self.configuracio = this.scene.get('Configuracio')
    
    var velocitatPilota = 15;
    if (lletraW.isDown && pala.y > 55) {
        pala.y = pala.y - velocitat * delta / 1000;
    } else if (lletraS.isDown && pala.y < 545) {
        pala.y = pala.y + velocitat * delta / 1000;
    }




    if (cursor.up.isDown && pala2.y > 55) {
        pala2.y = pala2.y - velocitat * delta / 1000;
    } else if (cursor.down.isDown && pala2.y < 545) {
        pala2.y = pala2.y + velocitat * delta / 1000;
    }

    if (pilota.x <= 25.6) {
        contador2++;
        puntuacio2.setText(contador2);
    } else if (pilota.x > 774.4) {
        contador1++;
        puntuacio1.setText(contador1);
    }

    if (contador1 >= puntuaciototal || contador2 >= puntuaciototal) {
        if (this.scene.get('Vencedor')) {
            game.scene.wake('Vencedor');
            game.scene.start('Vencedor');
            game.scene.sleep('Joc');
            game.scene.sleep('Botons');
            puntuacio1.setText('0');
            puntuacio2.setText('0');

        } else {
            game.scene.add('Vencedor', Vencedor);
            game.scene.start('Vencedor');
            game.scene.sleep('Joc');
            game.scene.sleep('Botons');
            puntuacio1.setText('0');
            puntuacio2.setText('0');
        }

    }
    self.v = new Phaser.Math.Vector2(pilota.body.velocity);

    if (v.x < 1.5 && v.x > 0) {
        v.x = 5;
    } else if (v.x < 0 && v.x > -1.5) {
        v.x = -5;
    }
    if (v.x == 0 && v.y == 0) {
        pilota.setVelocity(10);
    } else {
        var velNova = v.normalize();
        velNova.scale(velocitatPilota);
    }
    pilota.setVelocity(velNova.x, velNova.y);
}
}