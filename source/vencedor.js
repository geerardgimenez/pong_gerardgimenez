class Vencedor extends Phaser.Scene {
    constructor() {
        super({ key: 'Vencedor', active: true });
        self.botoTornar;
        self.botoComençar;
        self.joc;
        self.vencedor;
    }

    preload() {
        this.load.image('tornar', 'assets/retroceder.png');
        this.load.image('play', 'assets/play.png');
    }

    create() {
        self.joc = this.scene.get('Joc')
        if (contador1 > contador2) {
            self.vencedor = this.add.text(200, 275, 'El jugador blau ha guanyat!', { font: '25px Courier', fill: 'blue' });
        } else if (contador2 > contador1) {
            self.vencedor = this.add.text(200, 275, 'El jugador vermell ha guanyat!', { font: '25px Courier', fill: 'red' });
        }
        self.botoTornar = this.matter.add.image(400, 150, 'tornar');
        botoTornar.setInteractive();
        botoTornar.setScale(.2);

        self.botoComençar = this.matter.add.image(400, 400, 'play');
        botoComençar.setInteractive();
        botoComençar.setScale(.2);

        botoTornar.on('pointerdown', function (event) {
            game.scene.start('Menu');
            //this.scene.setVisible(false, 'Joc');
            game.scene.stop('Joc');
            this.scene.setVisible(false, 'Botons');
            this.scene.setVisible(false, 'Vencedor');
            this.scene.sleep('Vencedor');
            contador1 = 0;
            contador2 = 0;
        }, this)

        botoComençar.on('pointerdown', function (event) {
            this.scene.stop('Joc');
            this.scene.start('Joc');
            this.scene.sleep('Vencedor');
            this.scene.setVisible(false, 'Menu');
            this.scene.stop('Botons');
            this.scene.start('Botons');
            contador1 = 0;
            contador2 = 0;
            pilota.x = 400;
            pilota.y = 300;
        }, this)

    }

    update(time, delta) {

    }
}