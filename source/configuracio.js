class Configuracio extends Phaser.Scene {
    constructor() {
        super({ key: 'Configuracio', active: true });
        self.botoTornar;
        self.textpuntuacio;
        self.puntuacio3;
        self.puntuacio5;
        self.puntuacio10;
        self.puntuaciototal;

        self.quadrat;
        self.quadrat2;
        self.quadrat3;
        self.quadrat4;

        self.textforma1;
        self.migcercle;
        self.concau;
        self.rectangle;
        self.forma1

        self.textforma2;
        self.migcercle2;
        self.concau2;
        self.rectangle2;
        self.forma2

        self.obstacles;
        self.obssi;
        self.obsno;
        self.obstaclesfinal;

    }

    preload() {
        this.load.image('tornar', 'assets/retroceder.png');
        this.load.image('quadrat', 'assets/quadrat.png');
        this.load.image('jugador1', 'assets/rectangle.png');
        this.load.image('jugador2', 'assets/rectangle.png');
        this.load.image('migcercle', 'assets/migcircle.png');
        this.load.image('migcercle2', 'assets/migcircle2.png');
        this.load.image('concau', 'assets/concau.png');
        this.load.image('concau2', 'assets/concau2.png');
        this.load.image('rectangle', 'assets/rectangle.png');
        this.load.image('rectangle2', 'assets/rectangle2.png');

    }

    create() {
        self.botoTornar = this.matter.add.image(80, 80, 'tornar');
        botoTornar.setInteractive();
        botoTornar.setScale(.2);

        botoTornar.on('pointerdown', function (event) {
            game.scene.wake('Menu');
            game.scene.sleep('Configuracio');
        }, this)

        self.textpuntuacio = this.add.text(50, 150, 'Puntuació: ', { font: '20px Courier', fill: '#00ff00' });
        self.puntuacio3 = this.add.text(225, 150, '3', { font: '20px Courier', fill: '#00ff00' });
        puntuacio3.setInteractive();

        puntuacio3.on('pointerdown', function (event) {
            self.puntuaciototal = "3";
            if (self.quadrat) {
                quadrat.x = 232;
                quadrat.y = 160;
            } else {
                self.quadrat = this.add.image(232, 160, 'quadrat');
                quadrat.setScale(.06);
            }
        }, this)

        self.puntuacio5 = this.add.text(400, 150, '5', { font: '20px Courier', fill: '#00ff00' });
        puntuacio5.setInteractive();

        puntuacio5.on('pointerdown', function (event) {
            self.puntuaciototal = "5";
            if (self.quadrat) {
                quadrat.x = 408;
                quadrat.y = 160;
            } else {
                self.quadrat = this.add.image(408, 160, 'quadrat');
                quadrat.setScale(.06);
            }
        }, this)
        self.puntuacio10 = this.add.text(575, 150, '10', { font: '20px Courier', fill: '#00ff00' });
        puntuacio10.setInteractive();
        
        puntuacio10.on('pointerdown', function (event) {
            self.puntuaciototal = "10";
            if (self.quadrat) {
                quadrat.x = 588;
                quadrat.y = 160;
            } else {
                self.quadrat = this.add.image(588, 160, 'quadrat');
                quadrat.setScale(.06);
            }
        }, this)

        self.textforma1 = this.add.text(50, 250, 'Forma Jugador 1: ', { font: '20px Courier', fill: '#00ff00' });
        self.migcercle = this.add.image(350, 250, 'migcercle');
        migcercle.setScale(.11);
        migcercle.setInteractive();

        migcercle.on('pointerdown', function (event) {
            self.forma1 = "0";
            if (self.quadrat2) {
                quadrat2.x = 346;
                quadrat2.y = 250;
            } else {
                self.quadrat2 = this.add.image(346, 250, 'quadrat');
                quadrat2.setScale(.1);
            }
        }, this)

        self.concau = this.matter.add.image(500, 250, 'concau');
        concau.setScale(.06);
        concau.setInteractive();

        concau.on('pointerdown', function (event) {
            self.forma1 = "1";
            if (self.quadrat2) {
                quadrat2.x = 500;
                quadrat2.y = 250;
            } else {
                self.quadrat2 = this.add.image(500, 250, 'quadrat');
                quadrat2.setScale(.1);
            }
        }, this)

        self.rectangle = this.matter.add.image(650, 250, 'rectangle');
        rectangle.setScale(.32, .13);
        rectangle.rotation = 90.0 * (3.1459 / 180);
        rectangle.setInteractive();

        rectangle.on('pointerdown', function (event) {
            self.forma1 = "2";
            if (self.quadrat2) {
                quadrat2.x = 650;
                quadrat2.y = 250;
            } else {
                self.quadrat2 = this.add.image(650, 250, 'quadrat');
                quadrat2.setScale(.1);
            }
        }, this)

        self.textforma2 = this.add.text(50, 350, 'Forma Jugador 2: ', { font: '20px Courier', fill: '#00ff00' });
        self.migcercle2 = this.matter.add.image(350, 350, 'migcercle2');
        migcercle2.setScale(.11);
        migcercle2.setInteractive();
        migcercle2.on('pointerdown', function (event) {
            self.forma2 = "0";
            if (self.quadrat3) {
                quadrat3.x = 346;
                quadrat3.y = 350;
            } else {
                self.quadrat3 = this.add.image(346, 350, 'quadrat');
                quadrat3.setScale(.1);
            }
        }, this)

        self.concau2 = this.matter.add.image(500, 350, 'concau2');
        concau2.setScale(.06);
        concau2.setInteractive();
        concau2.on('pointerdown', function (event) {
            self.forma2 = "1";
            if (self.quadrat3) {
                quadrat3.x = 500;
                quadrat3.y = 350;
            } else {
                self.quadrat3 = this.add.image(500, 350, 'quadrat');
                quadrat3.setScale(.1);
            }
        }, this)
        self.rectangle2 = this.matter.add.image(650, 350, 'rectangle2');
        rectangle2.setScale(.32, .13);
        rectangle2.rotation = 90.0 * (3.1459 / 180);
        rectangle2.setInteractive();
        rectangle2.on('pointerdown', function (event) {
            self.forma2 = "2";
            if (self.quadrat3) {
                quadrat3.x = 650;
                quadrat3.y = 350;
            } else {
                self.quadrat3 = this.add.image(650, 350, 'quadrat');
                quadrat3.setScale(.1);
            }
        }, this)

        self.obstacles = this.add.text(50, 450, 'Obstacles: ', { font: '20px Courier', fill: '#00ff00' });
        self.obssi = this.add.text(300, 450, 'Si ', { font: '20px Courier', fill: '#00ff00' });
        obssi.setInteractive();
        obssi.on('pointerdown', function (event) {
            self.obstaclesfinal = "1";
            if (self.quadrat4) {
                quadrat4.x = 310;
                quadrat4.y = 460;
            } else {
                self.quadrat4 = this.add.image(310, 460, 'quadrat');
                quadrat4.setScale(.06);
            }
        }, this)
        self.obsno = this.add.text(500, 450, 'No ', { font: '20px Courier', fill: '#00ff00' });
        obsno.setInteractive();
        obsno.on('pointerdown', function (event) {
            self.obstaclesfinal = "0";
            if (self.quadrat4) {
                quadrat4.x = 510;
                quadrat4.y = 460;
            } else {
                self.quadrat4 = this.add.image(510, 460, 'quadrat');
                quadrat4.setScale(.06);
            }
        }, this)
    }

    update(time, delta) {

    }
}